#!/bin/bash

EXPT=$1
DIR=/basin/prog/connectivity/cms-polab/cms-master/expt
releasefile=$DIR/input_$EXPT/release_$EXPT.xyz
polyfile=$DIR/input_$EXPT/poly_$EXPT.xyz

#startday=124 #VIPlarge
#endday=300 #VIPlarge
#startday=-550 #VIP
#endday=-366
# startday=730 #Davao 2012
# endday=911
#startday=365 #Lanuza 2011
#endday=729
#startday=730 #Lanuza 2012
#endday=1095

#startday=0
#endday=20

releaseinterval=7
releasenumber=1

if [ ! -e $DIR/input_$EXPT/release_$EXPT.xyz ]; then
	echo 1>&2 "$0: $DIR/input_$EXPT/release_$EXPT.xyz doesn't exist"
	exit 1;
fi

#scripts/releaseCalendar.sh releasefile startday endday releaseinterval releasenumber
scripts/releaseCalendar.sh $releasefile $startday $endday $releaseinterval $releasenumber > $DIR/input_$EXPT/releaseFile ;

RELEASECODE=$?

if [[ $RELEASECODE != 0 ]]; then
	echo 1>&2 "$0: invalid output of releaseCalandar.sh"
	exit 1;
fi

#if [ ! -e $DIR/input_$EXPT/poly_$EXPT.xyz ]; then
#	echo 1>&2 "$0: $DIR/input_$EXPT/poly_$EXPT.xyz doesn't exist"
#	exit 1;
#fi

polycount=$(awk -F $' ' 'END{print $3}' $polyfile)

echo cmsfolderrun: $EXPT  > lastnotes
echo batchrundate: $(date) >> lastnotes
echo releasefile: $releasefile >> lastnotes
echo startday $(date -d "2015-01-01 $startday days" '+%Y %m %d') >> lastnotes
echo endday $(date -d "2015-01-01 $endday days" '+%Y %m %d') >> lastnotes
echo startday from 2015-01-01: $startday >> lastnotes
echo endday from 2015-01-01: $endday >> lastnotes
echo releaseinterval: $releaseinterval >> lastnotes
echo releasenumber: $releasenumber >> lastnotes
echo polycount: $polycount >> lastnotes

cat lastnotes

read -r -p "Continue? [y/N] " response
response=${response,,}    # tolower
if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]; then
	echo ""
else
	echo 1>&2 "$0: User cancelled";
	echo "User cancelled" >> lastnotes
	exit 1;
fi

mv lastnotes $DIR/expt_$EXPT/notes

read -r -p "Starting slurm run for $1, deleting previous data on expt_$1. Are you sure? [y/N] " response
response=${response,,}    # tolower
if [[ $response =~ ^(yes|y)$ ]]
then
        rm -rf $DIR/expt_$EXPT/output/ ;
        rm -rf $DIR/expt_$EXPT/SCRATCH/ ;
        rm -rf slurm* ;
else
        echo 1&>2 "$0: User cancelled";
        exit 1: 
fi

sbatch -n 14 -p basincluster --wrap="mpiexec ./cms $EXPT"
sleep 1; 
watch -n 1 squeue; 
tail slurm*;
