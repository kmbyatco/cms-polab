#!/bin/bash

# $1 = source NC file
# $2 = directory location
# $3 = starting day ( relative to 20100101)
# $4 = end day ( relative to 20100101)


DDAYS=$3;
NDAYS=$4;

SBASENAME=$(basename "$1")
SDIRNAME=$(dirname $(realpath "$1"))
DDIRNAME=$2

DHOURS=0;
ncdump -h $1

echo Creating symlinks:
echo source: $ $SDIRNAME"/"$SBASENAME
echo desitination: $DDIRNAME"/nest_1_"$(date -d "2010-01-01 +$DDAYS days +$DHOURS hour" '+%Y%m%d%H%M%S')".nc"
echo startday: $(date -d "2010-01-01 +$DDAYS days")
echo endday: $(date -d "2010-01-01 +$NDAYS days")

read -r -p "Continue? [y/N] " response
response=${response,,}    # tolower
if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]; then
        echo ""
else
        echo 1>&2 "$0: User cancelled";
        echo "User cancelled" >> lastnotes
        exit 1;
fi


for ((j=$DDAYS; j<=NDAYS; j+=1));
do
	DHOURS=0
	for ((i=0; i<24; i+=1));
	do
		DFILENAME=$DDIRNAME"/nest_1_"$(date -d "2010-01-01 +$DDAYS days +$DHOURS hour" '+%Y%m%d%H%M%S')".nc"
		UDFILENAME=$DDIRNAME"/nest_1_"$(date -d "2010-01-01 +$DDAYS days +$DHOURS hour" '+%Y%m%d%H%M%S')"u.nc"
		VDFILENAME=$DDIRNAME"/nest_1_"$(date -d "2010-01-01 +$DDAYS days +$DHOURS hour" '+%Y%m%d%H%M%S')"v.nc"
		WDFILENAME=$DDIRNAME"/nest_1_"$(date -d "2010-01-01 +$DDAYS days +$DHOURS hour" '+%Y%m%d%H%M%S')"w.nc"
		ln -s $SDIRNAME"/"$SBASENAME $DFILENAME
		ln -s $SDIRNAME"/"$SBASENAME $UDFILENAME
		ln -s $SDIRNAME"/"$SBASENAME $VDFILENAME
		ln -s $SDIRNAME"/"$SBASENAME $WDFILENAME
		date -d "2010-01-01 +$DDAYS days +$DHOURS hour" '+%Y%m%d%H%M%S'
		((DHOURS++))
	done;
	((DDAYS++))
done;
