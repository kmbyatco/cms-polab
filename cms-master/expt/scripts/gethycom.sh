#!/bin/bash

#location=/basin/rmt/antarctic/labdata/modelout/hydro/hycom/SEAb0.04/expt_01.1/netcdf/ASTI/2016
location=/basin/rmt/antarctic/labdata/modelout/hydro/hycom/SEAb0.04/expt_01.1/data/ncdf/ASTI/2015/

for i in $( ls $location/uvel -1 ); do

	filename=$(basename $i)
	filename=${filename%.*}

	# double check $1, it dependes on the file name
	year=$( echo $filename | awk -F '_' '{print $2}' | grep -Po '\d+' )
	day=$( echo $filename | awk -F '_' '{print $3}' | grep -Po '\d+' )
	hour=$( echo $filename | awk -F '_' '{print $4}' | grep -Po '\d+' )
	
	lnname=raw/nest_1_$(date -d "$year-01-01 +$day days +$hour hour" '+%Y%m%d%H%M%S' ).nc
	lnuname=raw/nest_1_$(date -d "$year-01-01 +$day days +$hour hour" '+%Y%m%d%H%M%S' )u.nc
	lnvname=raw/nest_1_$(date -d "$year-01-01 +$day days +$hour hour" '+%Y%m%d%H%M%S' )v.nc
	
	ln -s $location/uvel/${filename::-4}_3zu.nc $lnname
	ln -s $location/uvel/${filename::-4}_3zu.nc $lnuname
	ln -s $location/vvel/${filename::-4}_3zv.nc $lnvname
	
	echo "Done with " $(date -d "$year-01-01 +$day days +$hour hour" '+%Y%m%d%H%M%S' )
done;
