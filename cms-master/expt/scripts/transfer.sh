#!/bin/bash

location=$1
exptdate=$2

rsync -aP expt_$location input_$location /basin/rmt/okhotsk/scratch/connectivity/rmtexpt/expt_${location}_${exptdate}/
ls /basin/rmt/okhotsk/scratch/connectivity/rmtexpt/expt_${location}_${exptdate}/

rm -rf expt_$location input_$location
