function makepoints( experimentname )

	pkg load netcdf
	pkg load image
	
	addpath( "../../postproc" );
	groupXY
	
	%experimentname="manilabay";
	
	folder=["../expt_", experimentname, "/nests"];
	
	%polygon grid size in meters, actual grid size must not be smaller than data grid size
	poly_gridsize = 7000;
	
	%polygons must have upper limit. CMS cannot process too many polgyons
	maxpolygons = 1000;
	
	longitude = ncread([ folder "/nest_1_20150504000000.nc"],"Longitude");
	latitude = ncread([ folder "/nest_1_20150504000000.nc" ],"Latitude");
	zu = ncread( [ folder "/nest_1_20150504000000.nc" ] ,"zu");
	zv = ncread( [ folder "/nest_1_20150504000000.nc" ],"zv");
	
	zu(:,:,2:end)=[];
	
	zu( ~isnan(zu) ) = 1;
	zu( isnan(zu) ) = 0;
	zu=~zu;
	
	zu_dilate=imdilate(zu,ones(3));
	zu_final = imsubtract(zu_dilate,zu);
	
	%imshow(zu_final);
	
	[i,j]=ind2sub( [length(longitude), length(latitude)], find(zu_final) );
	release_lonlat = [longitude(i),latitude(j)]';
	
	y2deg = 1/111111;
	x2deg = 1/( 111111*cos( mean(latitude)*pi/180 ) );
	
	poly_latdeg = poly_gridsize*y2deg;
	poly_londeg = poly_gridsize*x2deg;
	
	poly_latitude = [ latitude(1): poly_latdeg : latitude(end)+poly_latdeg ]';
	poly_longitude = [ longitude(1): poly_londeg : longitude(end)+poly_londeg  ]';
	
	
	[ mesh_longitude, mesh_latitude ] = meshgrid(poly_longitude,poly_latitude);
	mesh_voffset = size(mesh_longitude)(2);
	mesh_hoffset = size(mesh_longitude)(1);
	
	release_fid = fopen( ["../input_", experimentname,"/release_",experimentname,".xyz"], "w" );
	poly_fid = fopen( ["../input_", experimentname,"/poly_",experimentname,".xyz"], "w" );
	
	release = [];
	polygons = [];
	m = 0;
	for n = 1:numel(mesh_longitude)'
	
		if( ~mod(n, 1000) )
			printf( "Testing polygon gridcell %d\n", n);
			fflush(stdout);
		endif
		
		%do not get centers from bottom-most and right-most points as upper-right corners define a square
		if( ~mod(n,mesh_hoffset) | n > numel(mesh_longitude)-mesh_hoffset )
			continue
		endif
		
		release_bool = bsxfun(@ge,release_lonlat,[mesh_longitude(n); mesh_latitude(n)]) & bsxfun(@lt,release_lonlat,[mesh_longitude(n+mesh_hoffset); mesh_latitude(n+1)]);
		polyrelease_col = find( all(release_bool) );
		polyrelease_count = length( find( all(release_bool) ) );
		
		if( polyrelease_count > 0)
			
			m++;
	
			% printf( "\t Particles found, generating polygon %d from gridcell %d\n", m, n);
			% fflush(stdout);
		
			polyrelease_lonlat = release_lonlat( : , polyrelease_col )';
		
			release = [ release; polyrelease_lonlat ];
	
			polyl = mesh_longitude(n);
			polyr = mesh_longitude(n+mesh_hoffset);
			polyd = mesh_latitude(n);
			polyu = mesh_latitude(n+1);
		
			polygons = [ polygons; polyl, polyu ];
			polygons = [ polygons; polyl, polyd ];
			polygons = [ polygons; polyr, polyu ];
			polygons = [ polygons; polyr, polyd ];
			
		
			fprintf( release_fid, "%f %f %d\n", [ polyrelease_lonlat repmat( m, size(polyrelease_lonlat)(1), 1 ) ]' );
			
			% order is important, must be either clockwise or counter-clockwise to count as a polygon for CMS
			fprintf( poly_fid, "%f %f %d\n", polyl, polyu, m );
			fprintf( poly_fid, "%f %f %d\n", polyr, polyu, m );
			fprintf( poly_fid, "%f %f %d\n", polyr, polyd, m );
			fprintf( poly_fid, "%f %f %d\n", polyl, polyd, m );
	
			
			if( m > maxpolygons )
				printf("Generated polygon count too high (%d > %d), stopping.\n", m, maxpolygons);
				fflush(stdout);
				return
			endif
			
		endif
		
	endfor
	
	fclose( release_fid );
	fclose( poly_fid );
	
	printf("Generated polygons: %d (max %d)\n", m, maxpolygons);
	fflush(stdout);
	
	hold on;
	scatter( release(:,1), release(:,2), 'blue', "." );
	scatter( polygons(:,1), polygons(:,2), 'red',  "." );
	axis equal

	pause();
endfunction
