#!/bin/bash

if [ $# -lt 5 ]; then
  echo 1>&2 "$0: not enough arguments"
  echo 1>&2 "$0 ReleaseSourceFile StarDayfrom20100101 EndDayfrom20101010 ReleaseIntervals NumberofParticlesperRelease"
  exit 2
elif [ $# -gt 5 ]; then
  echo 1>&2 "$0: too many arguments"
  echo 1>&2 "$0 ReleaseSourceFile StarDayfrom20100101 EndDayfrom20101010 ReleaseIntervals NumberofParticlesperRelease"
  exit 2
fi

RELEASESOURCE=$1;
DDAYS=$2;
NDAYS=$3;
JDAYS=$4;
NRELEASE=$5;

for ((i=$DDAYS; i<$NDAYS; i+=$JDAYS));
do
	awk -v nrelease="$NRELEASE" -v date="$(date -d "2015-01-01 $DDAYS days" '+%Y %m %d')" -F ' ' '{ print $3, $1, $2, '10', nrelease, date, '0'}' $RELEASESOURCE;
	((DDAYS+=$JDAYS));
done;

exit 0;
