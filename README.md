Localized CMS Suite for the POLAB "basin" cluster

This version of CMS is customized for running inside the laboratory beowulf cluster. 
The repo hosts: 
  1. Customized plottings scripts for automated plot-generation fo local Philippine areas
  2. An extensively tested experiment (input_davao) as a template for other CMS runs

To ensure reliable runs, use only latest tags instead of the bleeding-edge commit
=======
# connectivity-modeling-system (CMS), for more information please read and cite Paris et al. 2013EMS

  The CMS is a multiscale stochastic Lagrangian framework developed by Paris' Lab at the Rosenstiel School of Marine &amp; Atmospheric Science to study complex migrations and give probability estimates of dispersion, connectivity, fate of pollutants, and other Lagrangian phenomena. This repository facilitates and encourages COMMUNITY CONTRIBUTIONS TO CMS MODULES.

The CMS is an open-source Fortran toolbox for the multi-scale tracking of biotic and abiotic particles in the ocean. The tool is inherently multiscale, allowing for the seamless moving of particles between grids at different resolutions.

The CMS has been used on velocity fields from OFES, HYCOM, NEMO, MITgcm, UVic, ECCO2, SOSE, MOM and many other ocean models to computation dispersion, connectivity and fate in applications including large scale oceanography, marine reserve planning, and movement of marine biota all across the world.

The CMS uses RK4 timestepping and tricubic interpolation and is designed to be modular, meaning that it is relatively easy to add additional `behaviours' on the particles. Modules distributed with the code include random walk diffusion, mortality, diel migration, and mixed layer mixing. 
